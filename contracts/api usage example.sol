// Sample contract using the API

contract mortal {
    /* Define variable owner of the type address*/
    address owner;

    /* this function is executed at initialization and sets the owner of the contract */
    function mortal() { owner = msg.sender; }

    /* Function to recover the funds on the contract */
    function kill() { if (msg.sender == owner) selfdestruct(owner); }
}

contract blockchain2emailAPI {function SendEmail(string x, string y) returns(bool) {}}

contract MyContract is mortal{

	function SendEmail(string EmailAddress, string Message) internal returns (bool){
		return (blockchain2emailAPI(0xde5ebd0b8879b0a42b23b37e4d76a5e21a0bef4b).SendEmail.value(1000000000000000)(EmailAddress, Message));
	}
	function MyFunction(){
		SendEmail("example@email.com", "Your message goes here!");
	}
}



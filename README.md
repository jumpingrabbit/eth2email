# blockchain2email.com installation and usage

## Installation on the server
Optimised for an Ubuntu server on Amazon

1. Create a new EC2 instance (Ubuntu "micro" is fine), connect to it, and run the following installation code:
```
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install apache2 php ethereum nodejs libapache2-mod-php php-mcrypt php-mysql npm
```
2. Check you have nodejs version 6 installed (refer here: https://askubuntu.com/questions/786272/why-does-installing-node-6-x-on-ubuntu-16-04-actually-install-node-4-2-6 )
2. In Amazon EC2, change security group settings to allow all connections (TCP and UPD) on ports 30303, 30301, 123, 80
3. Place files from `/var/www/` onto the server.
4. Place `node-sender.js` into `/home/ubuntu/`.
5. Transfer the code from `/etc/apache2/sites-avaialble/eth2email.conf` into the default apache2 configuration (`/etc/apache2/sites-avaialble/DEFAULT.conf`)
6. Get Amazon SES (email service) passcodes and paste into:
   * `/var/www/html/scripts/contactus.php` (lines 8 and 9)
   * `node-sender.js` (lines 38 and 39)
7. Create a new MySQL instance on Amazon RDS and upload the `dump.sql` SQL dump into it (the dump is actually quite small, so you can simply execute each command to create the necessary database structure)
8. Edit database credentials in the `*.php` files and `node-sender.js`
9. Install nodejs modules: 
```
sudo npm install nodemailer web3 mysql
```
10. Deploy the Solidity contract from your eth account (`contracts/contract.sol`)
11. Update references to the new contract in the `*.php` files and `node-sender.js`

## Usage

1. Launch geth in a separate screen:
```
screen
geth --light console
```
2. After geth has started syncronisation, start RPC and WS. Type the following into geth console:
```
admin.startRPC();
admin.startWS("127.0.0.1", 8546, "*", "web3,net,eth");
```
3. Press Ctrl+A then Ctrl+D to exit from the screen
4. Launch node-sender.js in a separate screen:
```
screen
nodejs /home/ubuntu/node-sender.js
```
5. Should be all ready now!

## Misc
* Here are some useful functions for geth:
  * Set default account: `web3.eth.defaultAccount = web3.eth.accounts[0];`
  * Check balances of your accounts: `function checkAllBalances() { var i =0; web3.eth.accounts.forEach( function(e){ console.log("  web3.eth.accounts["+i+"]: " +  e + " \tbalance: " + web3.fromWei(web3.eth.getBalance(e), "ether") + " ether"); i++; })}; checkAllBalances() ;`
  * Check sync status: `var sync = web3.eth.syncing;sync;`
 
 

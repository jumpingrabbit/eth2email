<?php 
	
	// This page serves to allow email recipients to opt out of receiving email from the service
	
	$conn = new mysqli("localhost", "bc2emluser", "mistery76", "bc2eml");
	if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error);} 
	
	$key=$_GET["key"];
	
	$request=$conn->prepare("SELECT `email`,`nosend` FROM `emails` WHERE `key`= ?");
	$request->bind_param('s', $key);
	$request->execute();
	$result = $request->get_result();
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$email = $row["email"];
			$nosend = $row["nosend"];
		}
	}else{
		die("Wrong email key supplied.");
	}
	
?>
<html>
<head>
<title>blockchain2email.com: Send emails from Ethereum</title>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

<style type="text/css">
      body {margin: 0; padding: 0; font-family: 'Roboto Condensed', sans-serif;}
	  .highlighted { background-color:#000; color: #fff; }
	  h1{-webkit-margin-after: 0;-webkit-margin-before: 0; }
	  .wrapper{width:100%; padding:5px;}
	  .title{color: #eee; background-color:#333;}
	  .variable{color: #880000;}
	  #returnmessage{color: green;}
</style>
</head>
<body>
<?php include_once("scripts/analyticstracking.php") ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<table class="highlighted wrapper"><tr><td><h1><b class="title">blockchain2email.com:</b> Send emails from Ethereum</h1></td></tr></table>
<table  class="wrapper"><tr><td><p><b class="highlighted"><?php echo $email ?>:</b> <?php 
	if($nosend==0){
		echo "You are currently <span style='color:green;'>set up to receive emails</span> to this address.";
	}else{
		echo "You are currently set up to <span style='color:red;'>block emails</span> to this address. (Ethereum messages can still be dispatched, but you will not receive them by email.)";
	}?></p>
<p><button id="button1"><?php 
	if($nosend==0){
		echo "Block emails";
	}else{
		echo "Enable emails";
	}?></button>

</td></tr></table>
	

<script>
	var nosend=<?php echo $nosend ?>;
	
	$('#button1').on('click', function(el){
		$('#button1').html("Sending request...");
		$('#button1').prop('disabled', true);

		nosend = (nosend + 1) % 2;
		
			  
		$.ajax({
			type: "POST",
			url: "scripts/nosend.php",
			data: {key : '<?php echo $key ?>',
				nosend : nosend}, 
			cache: false,
			success: function(response1){
				if(response1=="success"){
					location.reload();	
				}else{
					$('#button1').html("An error has occured");
				}							
			},
			error: function(){
				$('#button1').html("An error has occured");
			}
		});
		
		
	});
	
</script>

</body>
</html>
<?php
	$conn->close();
?>
<html>
	<head>
		<title>blockchain2email.com: Send emails from Ethereum</title>
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		
		<style type="text/css">
			body {margin: 0; padding: 0; font-family: 'Roboto Condensed', sans-serif;}
			.highlighted { background-color:#000; color: #fff; }
			h1{-webkit-margin-after: 0;-webkit-margin-before: 0; }
			.wrapper{width:100%; padding:5px;}
			.title{color: #eee; background-color:#333;}
			.variable{color: #880000;}
			#returnmessage{color: green;}
		</style>
	</head>
	<body>
		<?php include_once("scripts/analyticstracking.php") ?>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		
		
		<link rel="stylesheet" href="hl-styles/default.css">
		<script src="scripts/highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
		
		<script src="scripts/contactus.js"></script>
		
		<table class="highlighted wrapper">
			<tr>
				<td>
					<h1><b class="title">blockchain2email.com:</b> Send emails from Ethereum</h1>
				</td>
			</tr>
		</table>
		<table  class="wrapper">
			<tr>
				<td>
					<p>You can now send messages directly from your Ethereum account or smart contract to email. Currently in alpha.</p>
					
					
					<p><b class="highlighted">Send your message from here with this &#x00D0;App:</b></p>
					<div id="cabbage">
						<p>Checking your browser...</p>
					</div>
					<script>
						function displayDAPP(){
							if (typeof web3 !== 'undefined') {
								$("#cabbage").load("scripts/insertdapp.php");
								}else{
								$("#cabbage").html("<p>Open this webpage in the <a href='https://github.com/ethereum/mist/releases' target='_blank'>Ethereum Mist browser</a>, Chrome with <a href='https://metamask.io/' target='_blank'>MetaMask plugin</a>, or another &#x00D0;App client to send messages directly from the web.</p>");
								setTimeout(displayDAPP,500);
							}		
						}
						displayDAPP();
					</script>
					
					<p><b class="highlighted">Send emails from a smart contract:</b> Include the following Solidity code into your smart contract:</p>
					
					<pre><code class="js">// Include the API reference
						contract blockchain2emailAPI {function SendEmail(string x, string y) returns(bool) {}}
						
						// Your contract code goes here:
						contract MyContract {
						
						// Keep this function as is. The functions returns whether the email has been sent successfully (i.e., if you paid at least 1 finney)
						function SendEmail(string EmailAddress, string Message) internal returns (bool){
						return (blockchain2emailAPI(0xde5ebd0b8879b0a42b23b37e4d76a5e21a0bef4b).SendEmail.value(1000000000000000)(EmailAddress, Message));
						}
						
						function MyFunction() {
						
						// Use this syntax to send your message
						SendEmail("example@email.com", "Your message goes here!");
						
						}
						
					}</code></pre>
					
					<p><b class="highlighted">Send emails from Ethereum Wallet:</b> If you'd like to test how it works in Ethereum Wallet, you can send a simple email directly from Ethereum Wallet. First, set up to watch the following contract:</p>
					<ul>
						<li>Name: <span class="variable">blockchain2email</span></li>
						<li>Address: <span class="variable">0xde5ebd0b8879b0a42b23b37e4d76a5e21a0bef4b</span></li>
					<li>JSON interface: <span class="variable">[{"constant":false,"inputs":[{"name":"EmailAddress","type":"string"},{"name":"Message","type":"string"}],"name":"SendEmail","outputs":[],"type":"function"}]</span></li></ul>
					<p>Then, send your message using the SendEmail function of this contract (along with 0.001 ether, i.e. 1 finney) and check your email. </p>
					
					<p><b class="highlighted">Two notes of caution:</b></p>
					<ul><li>The service is in alpha-testing and might not work perfectly at the moment</li>
					<li>Currently, there is no privacy with this API: the message and the email address remain in the blockchain unencrypted and can be seen by others</li></ul>
					
					<form id="form">
						<p><b class="highlighted">Contact me with feedback or questions:</b></p>
						<p id="returnmessage2" style='color:green;'></p>
						<p><label>From: </label><input type="email" id="email" placeholder="Your email address" size="30"/></p>
						<p><label>Message:</label><br>
						<textarea id="message" placeholder="Your message"  rows="5" cols="70"></textarea></p>
						<p><input type="button" id="submit" value="Send Message"/></p>
					</form>
					
					<p><b class="highlighted">Interested in real-world stuff that I do?</b></p>
					<p>Check out <a href="http://conjoint.online/">Conjoint.ly</a>, the easy conjoint analysis tool for market research.</p>
					
					
				</td>
			</tr>
		</table>
		<a href="https://www.sideprojectors.com/project/project/6081/blockchain2emailcom" alt="blockchain2email.com for sale at SideProjectors"><img style="position:fixed;z-index:1000;top:-5px; right: 20px; border: 0;" src="/badge_2_red.png" alt="This project blockchain2email.com is sale at SideProjectors"></a>
	</body>
</html>

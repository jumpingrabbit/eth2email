$(document).ready(function() {
    $("#submit").click(function() {
        var email = $("#email").val();
        var message = $("#message").val();
        $("#returnmessage2").html("<span style='color:black;'>Sending...</span>");
        // Checking for blank fields.
        if (message == '' || email == '') {
            $("#returnmessage2").html("<span style='color:red;'>Please fill both fields</span>");
        } else {
            // Returns successful data submission message when the entered information is stored in database.
            $.post("scripts/contactus.php", {
                email1: email,
                message1: message,
            }, function(data) {
                $("#returnmessage2").html(data); // Append returned message to message paragraph.
                if (data == "Your message has been sent. Thank you for contacting me") {
                    $("#form")[0].reset(); // To reset form fields on success.
                }
            });
        }
    });
});
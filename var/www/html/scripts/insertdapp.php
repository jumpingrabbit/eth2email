<div id="meat">
	<p>From: <span id="from"></span></p>
	<p><label>To: </label><input type="email" id="email" placeholder="Recepient email address" size="30"/></p>
	<p><label>Message:</label><br>
	<textarea id="message" placeholder="Your message"  rows="5" cols="70"></textarea></p>
	<p><input type="button" id="submitsend" value="Send Message"/></p>
	<p id="returnmessage"></p>
</div>
<script>
	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	$(document).ready(function() {
		
		if (typeof web3 !== 'undefined') {
			var SendEmailContract = web3.eth.contract([{"constant":false,"inputs":[{"name":"EmailAddress","type":"string"},{"name":"Message","type":"string"}],"name":"SendEmail","outputs":[{"name":"","type":"bool"}],"payable":true,"type":"function"}]).at("0xde5ebd0b8879b0a42b23b37e4d76a5e21a0bef4b");
			
			if(!web3.eth.accounts[0]){
				$("#meat").html("<span style='color:red;'>You are not signed in. Please choose an Ethereum account to proceed.</span>");
				}else{
				$("#from").html(web3.eth.accounts[0]);
				
				web3.eth.getBalance(web3.eth.accounts[0],function(err, valuereturned){
					if(valuereturned<1000000000000000){
						$("#returnmessage").html("<span style='color:red;'>Insufficient funds in your Ethereum account</span>");
					}			
				})
				
				$(document).on("click", "#submitsend", function(){
					
					var email = $("#email").val();
					var message = $("#message").val();
					$("#returnmessage").html("<span style='color:black;'>Sending...</span>");
					if (message == '' || email == '') {
						$("#returnmessage").html("<span style='color:red;'>Please fill both fields</span>");
						} else {
						if(validateEmail(email)){
							response = SendEmailContract.SendEmail(email, message, {from: web3.eth.accounts[0], value: 1000000000000000},function(err, address) {
								if (err){
									$("#returnmessage").html("<span style='color:red;'>"+err+"</span>");
									}else{
									$("#returnmessage").html("Message sent. Transaction ID: <a href='https://etherscan.io/tx/"+address+"' target='_blank'>"+address+"</a>");
								}
							});
							}else{
							$("#returnmessage").html("<span style='color:red;'>Please enter a valid email address</span>");
						}
					}
				});
			}	
			
			}else{
			$("#meat").html("<span style='color:red;'>Please open this page in <a href='https://github.com/ethereum/mist/releases' target='_blank'>Ethereum Mist browser</a> or another &#x00D0;App client.</span>");
		}	
		
	});
</script>
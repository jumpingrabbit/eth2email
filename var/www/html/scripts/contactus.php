<?php

require_once 'Mail.php';
$smtpParams = array (
  'host' => 'email-smtp.us-east-1.amazonaws.com',
  'port' => '587',
  'auth' => true,
  'username' => 'AMAZON_SES_USERNAME',
  'password' => 'AMAZON_SES_PASSWORD'
);
$mail = Mail::factory('smtp', $smtpParams);

// Fetching Values from URL.
$email = $_POST['email1'];
$message = $_POST['message1'];
$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.
// After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

$headers = array (
	  'From' =>  'feedback@blockchain2email.com',
	  'To' => 'nikita.a.samoylov@gmail.com',
	  'Subject' => 'Feedback submission');

$mail->send("nikita.a.samoylov@gmail.com", $headers, "From: ".$email."\n\n===MESSAGE BEGINS===\n".$message);


echo "Your message has been sent. Thank you for contacting me";

} else {
echo "<span style='color:red;'>Invalid email address</span>";
}
?>
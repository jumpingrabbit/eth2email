var nodemailer = require('nodemailer');
var Web3 = require('web3');
var web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));

var mysql	   = require('mysql');
var connection;
function handleDisconnect() {
  connection = mysql.createConnection({
	host	   : 'localhost',
	user	   : 'bc2emluser',
	password : 'mistery76',
	database : 'bc2eml'
});
   connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}
handleDisconnect();

var smtpConfig = {
	host: 'email-smtp.us-east-1.amazonaws.com',
	port: 465,
	secure: true, // use SSL
	auth: {
		user: 'AMAZON_SES_USERNAME',
		pass: 'AMAZON_SES_PASSWORD'
	}
};
var transporter = nodemailer.createTransport(smtpConfig);

var randomString = function(length) {
	var text = "";
	var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
	for(var i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}
	

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;")
         .replace(/[\r\n]/g, "<br>");
}


var blockchain2emailsenderContract = web3.eth.contract([{"constant":false,"inputs":[],"name":"withdraw","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"EmailAddress","type":"string"},{"name":"Message","type":"string"}],"name":"SendEmail","outputs":[{"name":"","type":"bool"}],"type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"Sender","type":"address"},{"indexed":false,"name":"EmailAddress","type":"string"},{"indexed":false,"name":"Message","type":"string"}],"name":"EmailSent","type":"event"}]).at("0xde5ebd0b8879b0a42b23b37e4d76a5e21a0bef4b");

var EmailEvent = blockchain2emailsenderContract.EmailSent({}, {fromBlock: 'latest' , toBlock: 'latest'});	/// replace with an anchor block for stability later

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function sendEmail(email,key,sender,txhash,message) {
	
	var textmessage="NEW MESSAGE FROM ETHEREUM BLOCKCHAIN\n\nFrom "+sender+" <https://etherscan.io/address/"+sender+"> in transaction "+txhash+" <https://etherscan.io/tx/"+txhash+">\n\n===MESSAGE BEGINS===\n\n"+message+"\n\n====MESSAGE ENDS====\n\nThis email was delivered to you by http://blockchain2email.com/ (a smart contract API to send emails from Ethereum). If you would like to stop receiving these messages, please update your preferences here: http://blockchain2email.com/emailsetup.php?key="+ key;
			
	
	var htmlmessage="<html><body><p><b>From</b> <a href=https://etherscan.io/address/"+sender+" id='from'>"+sender+"</a> in transaction <a href=https://etherscan.io/tx/"+txhash+" id='txhash'>"+txhash+"</a></p><p><b>Message:</b></p><table bgcolor='#eee' width='100%'><tr width='100%'><td width='100%' id='message'>"+escapeHtml(message)+"</td></tr></table><br><hr><p>This email was delivered to you by <a href=http://blockchain2email.com/>blockchain2email.com</a> (a smart contract API to send emails from Ethereum). If you would like to stop receiving these messages, please update your preferences <a href=http://blockchain2email.com/emailsetup.php?key="+ key +">here</a>.</body></html>";
	
	var mailOptions = {
		from: sender+'@blockchain2email.com',  
		to: email, 
		subject: 'A message from the Ethereum blockchain', 
		text: textmessage,	
		html: htmlmessage
	};
	
	transporter.sendMail(mailOptions, function(error, info){
		if(error){
			return console.log(error);
		}
		console.log('Message sent: ' + info.response);
	});
	
}

EmailEvent.watch(function(error, result){
	if (!error){
		console.log("Email detected");
		if(validateEmail(result['args']['EmailAddress'])){
			var Query00 = connection.query('SELECT `key`,`nosend` from `emails` where `email`="' + result['args']['EmailAddress'] + '"', function(err, rows, fields) {
				if (!err) {
					if (rows.length == 0) {
						var key = randomString(20);
						var Query00 = connection.query('INSERT INTO `emails` (`id` ,`email` ,`key` ,`nosend`) VALUES (NULL ,  "' + result['args']['EmailAddress'] + '",  "' + key + '",  "0")');
						sendEmail(result['args']['EmailAddress'],key,result['args']['Sender'],result['transactionHash'],result['args']['Message']);
					} else {
						if (rows[0].nosend == 1) {
							console.log('Email is blocked: '+result['args']['EmailAddress']);
						} else {
							sendEmail(result['args']['EmailAddress'],rows[0].key,result['args']['Sender'],result['transactionHash'],result['args']['Message']);
						}
					}
				} else {
					console.log('Error while performing Query.');
				}
			});
			
		}else{
			console.log('Not an email address: ' + result['args']['EmailAddress']);
		}		
	}
	else{console.log("Error: "+error);}
});
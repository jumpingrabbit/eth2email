-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bc2eml
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(500) NOT NULL,
  `key` varchar(20) NOT NULL,
  `nosend` tinyint(1) NOT NULL DEFAULT '0',
  `entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`key`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES (7,'sdsf.sadfsdf@techie.com','3a2ajnxh01u48ish6pez',0,'0000-00-00 00:00:00'),(8,'qwrwqer@qedewr9723yh74h23f342f63gweqrwer.com','whrxbp66y0jjn57h8bdy',0,'0000-00-00 00:00:00'),(9,'wehfuwhefui@wefnwqoenfuirw.com','dpqoa9w9igsczzw6w4nl',0,'0000-00-00 00:00:00'),(10,'ewr43r43r43@wfrfrwrwermoi.werewr','rfz4i1plrzbkbe7a6uld',0,'0000-00-00 00:00:00'),(11,'ewfoiejfiownufewrnfu@wnrfuiwnrfuirn.cm','9wl9sxl488q0jsviw8br',0,'0000-00-00 00:00:00'),(12,'qweqrweqr@cjewfjkwemflk.comwe','0w6jxik5txhdfjb1e6a9',0,'0000-00-00 00:00:00'),(13,'e3242314234@ewr23412342334.ewr','xeglta8fpw4if22iuhkm',0,'0000-00-00 00:00:00'),(14,'gfdtgfdhgf@ghfhg.bu','4i3jm95pzs1vzpzmyvs1',0,'0000-00-00 00:00:00'),(15,'nick@gnidan.org','7lx4zqiw0kccvexg16n6',0,'0000-00-00 00:00:00'),(16,'kenlonseth@gmail.com','kjamub21ro5pbekjxael',0,'0000-00-00 00:00:00'),(17,'annatron@gmail.com','b914qybqwozgwsuo9tnn',0,'0000-00-00 00:00:00'),(18,'wengqingbin@365du.com','ovc29dl0antqx39x2jic',0,'0000-00-00 00:00:00'),(19,'wqb0039@qq.com','9uyd2nx24y25j38khbzi',0,'0000-00-00 00:00:00'),(20,'wally@twitter.com','rnpfppajn2bnyow6hy7m',0,'0000-00-00 00:00:00'),(21,'tester4566@mail.ru','w9vansy6awhd7rj37t2v',0,'2016-09-14 13:29:33'),(22,'malisa.pusonja@klotfrket.co','pvj1hd3igun50x4ip4qo',0,'2016-11-09 14:46:24'),(23,'pfvcuoad@sharklasers.com','ecjdn6asobl2or1pjn6z',0,'2017-02-04 07:40:41'),(24,'johndangerstorey@gmail.com','ydhsj4bszrisr5h7ni3u',0,'2017-05-21 01:00:55'),(25,'manuelgar@hotmail.com','rkre5cm0gwlf2aj5dbor',0,'2017-05-21 01:02:01'),(26,'jnbs77@gmail.com','ilk86tcdc7gb18iuzooo',0,'2017-05-21 01:03:23'),(27,'felixfidelio@gmail.com','3neechax64lejn8f54y7',0,'2017-05-21 01:03:31'),(28,'trapp2357@gmail.com','71humn3e48t63t72z08j',0,'2017-05-23 13:31:25'),(29,'sagev9000@gmail.com','a9i2awxxqfvm9o8d98e0',0,'2017-05-27 16:33:27'),(30,'mnks@p.9q.ro','5np6a7c39xo98ald14e0',0,'2017-06-22 03:22:19');
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-02  5:17:10
